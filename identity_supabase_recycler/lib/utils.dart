import 'package:flutter/material.dart';
import 'package:identity_supabase_recycler/colors.dart';

void showError(context, String message){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: const Text("Ошибка"),
    content: Text(message),
    actions: [
      GestureDetector(
          child: Text("OK", style: TextStyle(color: colorAccent)),
          onTap: () {
            Navigator.pop(context);
          }
      )
    ],
  ));
}