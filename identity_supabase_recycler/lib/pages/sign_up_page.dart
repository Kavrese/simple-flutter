import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:identity_supabase_recycler/connection.dart';
import 'package:identity_supabase_recycler/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import '../colors.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {

  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var confirmPasswordController = TextEditingController();

  void _tapSignUp(){
    var email = emailController.text;
    var password = passwordController.text;
    var confirmPassword = confirmPasswordController.text;

    if (!EmailValidator.validate(email)){
      showError(context, "Почта не по паттерну");
    }

    if (password.isEmpty){
      showError(context, "Введите пароль");
    }

    if (confirmPassword != password){
      showError(context, "Пароли не совпадают");
    }

    _signUp(email, password);
  }

  Future<void> _signUp(String email, String password) async {
    try {
      await client.auth.signUp(password: password, email: email);
    }on AuthException catch(error){
      showError(context, error.message);
    }catch(error){
      showError(context, "Неизвестная ошибка");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 22.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 80),
            Text("Создать аккаунт", style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: colorText)),
            const SizedBox(height: 8),
            Text("Введите почту и пароль для регистрации в приложении", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: colorSecondText)),
            const SizedBox(height: 92),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: colorHint)
              ),
              child: TextField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 12),
                    hintText: "Почта",
                    hintStyle: TextStyle(color: colorHint),
                    border: InputBorder.none
                ),
              ),
            ),
            const SizedBox(height: 18),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: colorHint)
              ),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 12),
                    hintText: "Пароль",
                    hintStyle: TextStyle(color: colorHint),
                    border: InputBorder.none
                ),
              ),
            ),
            const SizedBox(height: 18),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: colorHint)
              ),
              child: TextField(
                obscureText: true,
                controller: confirmPasswordController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 12),
                    hintText: "Повторите пароль",
                    hintStyle: TextStyle(color: colorHint),
                    border: InputBorder.none
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: _tapSignUp,
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size(double.infinity, 50),
                        backgroundColor: colorAccent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)
                        )
                    ),
                    child: const Text("Создать аккаунт", style: TextStyle(color: Colors.white, fontSize: 16)),
                  ),
                  const SizedBox(height: 12),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pop(context);
                    },
                    child: RichText(text: TextSpan(
                        children: [
                          TextSpan(text: "Уже есть аккаунт? ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: colorSecondText)),
                          TextSpan(text: "Войти!", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: colorAccent)),
                        ]
                    )),
                  ),
                  const SizedBox(height: 32),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
