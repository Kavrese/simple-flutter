import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:identity_supabase_recycler/colors.dart';
import 'package:identity_supabase_recycler/connection.dart';
import 'package:identity_supabase_recycler/pages/sign_in_page.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class DataPage extends StatefulWidget {
  const DataPage({Key? key}) : super(key: key);

  @override
  State<DataPage> createState() => _DataPageState();
}

class _DataPageState extends State<DataPage> {


  late StreamSubscription<AuthState> onAuthStateChange;
  List<Map<String, dynamic>> data = [];


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              itemCount: data.length,
              itemBuilder: (context, index) {
                return getWidgetElement(data[index]);
              }
            ),
          ),
          ElevatedButton(
            onPressed: (){
              client.auth.signOut();
            },
            style: ElevatedButton.styleFrom(
              minimumSize: const Size.fromHeight(50),
              backgroundColor: colorAccent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(0)
              )
            ),
            child: const Text("Выйти", style: TextStyle(color: Colors.white)),
          )
        ]
      )
    );
  }

  Widget getWidgetElement(Map<String, dynamic> elemData){
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 8, horizontal: 22),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox.square(
            dimension: 60,
            child: CircleAvatar(backgroundImage: NetworkImage(elemData["image_url"]!))
          ),
          const SizedBox(width: 18),
          Expanded(
            child: Text(
              elemData["title"],
              style: TextStyle(color: colorText, fontWeight: FontWeight.w500),
            ),
          ),
          const SizedBox(width: 18),
        ],
      ),
    );
  }

  Future<void> _updateData() async {
    List<Map<String, dynamic>> newData = await getData();
    setState(() {
      data = newData;
    });
  }

  @override
  void initState() {
    onAuthStateChange = client.auth.onAuthStateChange.listen((event) {
      final session = event.session;
      if (session == null && context.mounted){
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const SignInPage()), (Route<dynamic> route) => false);
      }
    });
    super.initState();
    _updateData();
  }

  @override
  void dispose() {
    onAuthStateChange.cancel();
    super.dispose();
  }
}
