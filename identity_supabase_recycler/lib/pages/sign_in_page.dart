import 'dart:async';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:identity_supabase_recycler/colors.dart';
import 'package:identity_supabase_recycler/connection.dart';
import 'package:identity_supabase_recycler/pages/data_page.dart';
import 'package:identity_supabase_recycler/pages/sign_up_page.dart';
import 'package:identity_supabase_recycler/utils.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({super.key});

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {

  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  late StreamSubscription<AuthState> onAuthStateChange;

  @override
  void initState() {
    onAuthStateChange = client.auth.onAuthStateChange.listen((event) {
      final session = event.session;
      if (session != null && context.mounted){
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_) => const DataPage()), (Route<dynamic> route) => false);
      }
    });
    super.initState();
  }


  @override
  void dispose() {
    onAuthStateChange.cancel();
    super.dispose();
  }

  void _tapLogin(){
    var email = emailController.text;
    var password = passwordController.text;

    if (!EmailValidator.validate(email)){
      showError(context, "Почта не по паттерну");
    }

    if (password.isEmpty){
      showError(context, "Введите пароль");
    }

    _login(email, password);
  }

  Future<void> _login(email, password) async {
    try {
      await client.auth.signInWithPassword(password: password, email: email);
    } on AuthException catch(error){
      showError(context, error.message);
    } catch (error){
      showError(context, "Неизвестная ошибка");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 22.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 80),
            Text("Войти в аккаунт", style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold, color: colorText)),
            const SizedBox(height: 8),
            Text("Введите почту и пароль для входа в приложение", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: colorSecondText)),
            const SizedBox(height: 92),
            Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                border: Border.all(color: colorHint)
              ),
              child: TextField(
                controller: emailController,
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.symmetric(horizontal: 12),
                  hintText: "Почта",
                  hintStyle: TextStyle(color: colorHint),
                  border: InputBorder.none
                ),
              ),
            ),
            const SizedBox(height: 18),
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: colorHint)
              ),
              child: TextField(
                obscureText: true,
                controller: passwordController,
                keyboardType: TextInputType.visiblePassword,
                decoration: InputDecoration(
                    contentPadding: const EdgeInsets.symmetric(horizontal: 12),
                    hintText: "Пароль",
                    hintStyle: TextStyle(color: colorHint),
                    border: InputBorder.none
                ),
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ElevatedButton(
                    onPressed: _tapLogin,
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size(double.infinity, 50),
                      backgroundColor: colorAccent,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)
                      )
                    ),
                    child: const Text("Войти", style: TextStyle(color: Colors.white, fontSize: 16)),
                  ),
                  const SizedBox(height: 12),
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => const SignUpPage()));
                    },
                    child: RichText(text: TextSpan(
                      children: [
                        TextSpan(text: "Нет аккаунта? ", style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500, color: colorSecondText)),
                        TextSpan(text: "Зарегистрируйся сейчас!", style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold, color: colorAccent)),
                      ]
                    )),
                  ),
                  const SizedBox(height: 32),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}