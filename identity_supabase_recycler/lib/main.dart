import 'package:flutter/material.dart';
import 'package:identity_supabase_recycler/config.dart';
import 'package:identity_supabase_recycler/pages/sign_in_page.dart';
import 'package:supabase_flutter/supabase_flutter.dart';

import 'connection.dart';
import 'pages/data_page.dart';

Future<void> main() async {
  await Supabase.initialize(url: url, anonKey: key);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Simple Flutter',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: client.auth.currentSession == null ? const SignInPage() : const DataPage(),
    );
  }
}
