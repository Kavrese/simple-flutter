import 'package:supabase_flutter/supabase_flutter.dart';

final client = Supabase.instance.client;

Future<List<Map<String, dynamic>>> getData() async {
  List<Map<String, dynamic>> data = await client.from("data").select();
  return data;
}